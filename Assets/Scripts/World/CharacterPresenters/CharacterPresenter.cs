using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Logic;
using Logic.Characters;
using UnityEngine;
using World.AStar;

namespace World.CharacterPresenters
{
    public class CharacterPresenter : MonoBehaviour
    {
        public CharacterModel Model { get; private set; }

        [SerializeField]
        private float _DistanceTolerance = 0.1f;
        
        [SerializeField, Tooltip("The higher value, the better result - might affect performance")]
        private int _BezierPathResolution = 20;

        [SerializeField]
        private AnimationCurve _SpeedSmoothingCurve;
        
        private Renderer _Renderer;
        private Coroutine _MoveRoutine = null;
        private Coroutine _FollowRoutine = null;

        private Path _CurrentPath;
        private Vector3 _CurrentVelocity;
        private IReadOnlyList<Vector3> _DebugPoints;
        
        
        public void Initialize(CharacterModel model)
        {
            Model = model;
            name = Model.name;
            _Renderer = GetComponent<Renderer>();
            _Renderer.material.color = Model.Color;
        }

        public void Move(Path path)
        {
            if (_MoveRoutine != null)
            {
                StopCoroutine(_MoveRoutine);
            }
            
            if (_FollowRoutine != null)
            {
                StopCoroutine(_FollowRoutine);
            }

            _CurrentPath = path;
            _MoveRoutine = StartCoroutine(MoveRoutine(_CurrentPath.GenerateSpline(transform.position, _BezierPathResolution, 0.75f), Model.MovementSpeed, 0));
        }

        public void Follow(Path path, int offset, float guideSpeed, float guideManeuverability)
        {            
            if (_MoveRoutine != null)
            {
                StopCoroutine(_MoveRoutine);
            }
            
            if (_FollowRoutine != null)
            {
                StopCoroutine(_FollowRoutine);
            }

            _CurrentPath = path;
            _FollowRoutine = StartCoroutine(MoveRoutine(_CurrentPath.GenerateSpline(transform.position, _BezierPathResolution, 0.75f, offset), guideSpeed, offset));
        }

        
        private IEnumerator MoveRoutine(List<Vector3> points, float movementSpeed, int pathOffset)
        {
            if (points.Count < 2)
            {
                yield break;
            }

            _DebugPoints = points;

            var startingNodeSearchIndex = (transform.position - _CurrentPath.Nodes[0].Position).sqrMagnitude < _DistanceTolerance * _DistanceTolerance ? 1 : 0;
            var startingNodeIndex = points.FindIndex(point => (_CurrentPath.Nodes[startingNodeSearchIndex].Position - point).sqrMagnitude < _DistanceTolerance * _DistanceTolerance);
            var endingNodeSearchIndex = ^Mathf.Clamp(2 + pathOffset, 0, points.Count);
            var endingNodeIndex = points.FindIndex(point => (_CurrentPath.Nodes[endingNodeSearchIndex].Position - point).sqrMagnitude < _DistanceTolerance * _DistanceTolerance);

            var aggregatedStartingDistance = GetSqrDistance(startingNodeIndex);
            var aggregatedEndingDistance = GetSqrDistance(points.Count, endingNodeIndex);
            
            for (var i = 0; i < points.Count; i ++)
            {
                var nextPosition = points[i];
                
                while ((transform.position - nextPosition).sqrMagnitude > _DistanceTolerance * _DistanceTolerance)
                {
                    var currentMovementSpeed = movementSpeed;

                    if (i < startingNodeIndex)
                    {
                        var currentSqrDistance = GetSqrDistance(i);
                        currentSqrDistance += (transform.position - nextPosition).sqrMagnitude;
                        currentMovementSpeed = Extensions.EaseOutQuad(0.1f, movementSpeed, currentSqrDistance / aggregatedStartingDistance);
                    }
                    else if (i > endingNodeIndex)
                    {
                        var currentSqrDistance = GetSqrDistance(i, endingNodeIndex);
                        currentSqrDistance += (transform.position - nextPosition).sqrMagnitude;
                        currentMovementSpeed = Extensions.EaseInQuad(movementSpeed, 0.1f, currentSqrDistance / aggregatedEndingDistance);
                    }
                    
                    transform.position = Vector3.MoveTowards(transform.position, nextPosition, currentMovementSpeed * Time.deltaTime);
                    
                    RotateTowards(Model.Maneuverability, nextPosition);
                    yield return null;
                }
            }

            _DebugPoints = null;
            yield break;

            float GetSqrDistance(int lastIndex, int startingIndex = 1)
            {
                if (lastIndex > points.Count)
                {
                    lastIndex = points.Count;
                }

                if (startingIndex < 1)
                {
                    startingIndex = 1;
                }

                var sqrDistance = 0f;
                
                for (var i = startingIndex; i < lastIndex; i++)
                {
                    sqrDistance += (points[i - 1] - points[i]).sqrMagnitude;
                }

                return sqrDistance;
            }
        }

        private void RotateTowards(float maneuverability, Vector3 nextPosition)
        {
            var direction = (nextPosition - transform.position).normalized;
            var newDirection = Vector3.RotateTowards(transform.forward, direction, maneuverability * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }
        
        private void OnDrawGizmosSelected()
        {
            if (_DebugPoints == null)
            {
                return;
            }

            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(_CurrentPath.EndPoint, 0.2f);
            
            Gizmos.color = Color.red;
            
            for (var i = 1; i < _DebugPoints.Count; i++)
            {
                Gizmos.DrawLine(_DebugPoints[i - 1], _DebugPoints[i]);
            }
        }
    }
}