using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.Characters;
using UnityEngine;
using UnityEngine.AddressableAssets;
using World.AStar;
using Random = UnityEngine.Random;

namespace World.CharacterPresenters
{
    public class CharacterPresentersController : MonoBehaviour
    {
        [SerializeField]
        private CharactersManager CharactersManager;
        
        [SerializeField]
        private AssetReference CharacterPresenterAssetReference;

        [SerializeField]
        private AStarController AStarController;
        
        [SerializeField]
        private List<CharacterPresenter> _Presenters = new();
        public IReadOnlyList<CharacterPresenter> Presenters => _Presenters;

        private bool _PresentersSpawned;
        
        private void Awake()
        {
            CharactersManager.OnCharactersSpawned += SpawnPresenters;
            AStarController.OnNodesGenerated += WarpPresenters;
        }

        public async void SpawnPresenters()
        {
            _PresentersSpawned = false;
            
            if (_Presenters.Count > 0)
            {
                for (var i = _Presenters.Count - 1; i >= 0; i--)
                {
                    var presenter = _Presenters[i];
                    _Presenters.Remove(presenter);
                    Destroy(presenter.gameObject);
                }
            }
            
            foreach (var characterModel in CharactersManager.SpawnedCharacters)
            {
                await SpawnPresenterFor(characterModel);
            }

            _PresentersSpawned = true;
        }

        public async Task SpawnPresenterFor(CharacterModel characterModel)
        {
            var randPos = Random.onUnitSphere * 5;
            randPos.y = 1;

            var instantiateOperation = CharacterPresenterAssetReference.InstantiateAsync(randPos, Quaternion.identity, transform);

            await instantiateOperation.Task;

            var presenter = instantiateOperation.Result.GetComponent<CharacterPresenter>();
            presenter.Initialize(characterModel);
            _Presenters.Add(presenter);
        }

        private void WarpPresenters()
        {
            foreach (var presenter in _Presenters)
            {
                AStarController.Warp(presenter.transform);
            }
        }

        public CharacterPresenter GetPresenterFor(CharacterModel model)
        {
            return _Presenters.FirstOrDefault(presenter => presenter.Model == model);
        }

        public async Task Load((string, Vector3, Quaternion)[] parsedSaveData)
        {
            while (!_PresentersSpawned)
            {
                await Task.Yield();
            }
            
            foreach (var data in parsedSaveData)
            {
                var presenter = _Presenters.FirstOrDefault(presenter => presenter.Model.ID == data.Item1);

                if (presenter != null)
                {
                    presenter.transform.SetPositionAndRotation(data.Item2, data.Item3);
                }

                await Task.Yield();
            }
        }

        private void OnDestroy()
        {
            CharactersManager.OnCharactersSpawned -= SpawnPresenters;
            AStarController.OnNodesGenerated -= WarpPresenters;
        }
    }
}