using System.Collections;
using Logic.Characters;
using Logic.SelectionSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using World.AStar;
using World.CharacterPresenters;

namespace World
{
    public class WorldController : MonoBehaviour
    {
        [SerializeField]
        private CharactersManager CharactersManager;
        
        [SerializeField]
        private CharacterPresentersController CharacterPresentersController;

        [SerializeField]
        private AStarController AStarController;

        [SerializeField]
        private PlayerInput PlayerInput;

        [SerializeField]
        private SelectableManager SelectableManager;

        [SerializeField, Tooltip("In seconds")]
        private float _FollowersMovementDelay = 1f;
        
        private Coroutine _DelayFollowersMovementRoutine;
        
        private void Start()
        {
            PlayerInput.currentActionMap.FindAction("Fire").performed += OnFirePerformed;
            AStarController.Initialize();
            CharactersManager.Initialize();
        }

        private void OnFirePerformed(InputAction.CallbackContext callbackContext)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            
            if (ReferenceEquals(SelectableManager.CurrentTarget, null))
            {
                return;
            }

            if (_DelayFollowersMovementRoutine != null)
            {
                StopCoroutine(_DelayFollowersMovementRoutine);
                _DelayFollowersMovementRoutine = null;
            }
            
            var mousePos = Mouse.current.position.ReadValue();
            var ray = Camera.main!.ScreenPointToRay(mousePos);
            var plane = new Plane(Vector3.up, Vector3.zero);

            if (!plane.Raycast(ray, out var enter))
            {
                return;
            }
                
            var hitPosition = ray.GetPoint(enter);

            if (SelectableManager.CurrentTarget is not CharacterModel guide)
            {
                return;
            }
            
            var guidePresenter = CharacterPresentersController.GetPresenterFor(guide);
            var path = AStarController.FindPath(guidePresenter.transform.position, hitPosition);

            if (path == null)
            {
                return;
            }

            guidePresenter.Move(path);
            _DelayFollowersMovementRoutine = StartCoroutine(DelayFollowersMovement(guidePresenter, path));
        }

        private IEnumerator DelayFollowersMovement(CharacterPresenter guidePresenter, Path path)
        {
            var offset = 1;
            var waitForSeconds = new WaitForSeconds(_FollowersMovementDelay);
            
            foreach (var characterPresenter in CharacterPresentersController.Presenters)
            {
                if (characterPresenter == guidePresenter)
                {
                    continue;
                }

                yield return waitForSeconds;
                var combinedPath = AStarController.FindPath(characterPresenter.transform.position, path.Nodes[0].Position);
                combinedPath.CombinePaths(path);
                characterPresenter.Follow(combinedPath, offset, guidePresenter.Model.MovementSpeed, guidePresenter.Model.Maneuverability);
                offset++;
            }
        }
    }
}