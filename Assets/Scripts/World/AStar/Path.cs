using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace World.AStar
{
    public class Path
    {
        private List<AStarNode> _Nodes;
        public IReadOnlyList<AStarNode> Nodes => _Nodes;
        
        public Vector3 EndPoint { get; private set; }
        
        public Path(List<AStarNode> nodes, Vector3 endPoint)
        {
            _Nodes = nodes;
            EndPoint = endPoint;
        }

        /// <summary>
        /// Adds given path to the end of this path
        /// </summary>
        /// <param name="pathToCombine">Path that will be added to this path</param>
        public void CombinePaths(Path pathToCombine)
        {
            var intersectingPart = _Nodes.Intersect(pathToCombine.Nodes);
            _Nodes.AddRange(pathToCombine.Nodes.Skip(1).Except(intersectingPart));
            EndPoint = pathToCombine.EndPoint;
        }

        // Source: https://andrewhungblog.wordpress.com/2017/03/03/catmull-rom-splines-in-plain-english/
        public List<Vector3> GenerateSpline(Vector3 startPosition, int stepsPerCurve = 3, float tension = 1, int offset = 0)
        {
            if (stepsPerCurve < 1)
            {
                stepsPerCurve = 1;
            }

            var points = _Nodes.Select(node => node.Position).ToList();
            points.Insert(0, startPosition);
            
            var result = new List<Vector3>();

            for (var i = 0; i < points.Count - 1 - offset; i++)
            {
                var prev = i == 0 ? points[i] : points[i - 1];
                var currStart = points[i];
                var currEnd = points[i + 1];
                var next = i == points.Count - 2 ? points[i + 1] : points[i + 2];

                for (var step = 0; step <= stepsPerCurve; step++)
                {
                    var t = (float)step / stepsPerCurve;
                    var tSquared = t * t;
                    var tCubed = tSquared * t;

                    var interpolatedPoint =
                        (-.5f * tension * tCubed + tension * tSquared - .5f * tension * t) * prev +
                        (1 + .5f * tSquared * (tension - 6) + .5f * tCubed * (4 - tension)) * currStart +
                        (.5f * tCubed * (tension - 4) + .5f * tension * t - (tension - 3) * tSquared) * currEnd +
                        (-.5f * tension * tSquared + .5f * tension * tCubed) * next;

                    if (!result.Contains(interpolatedPoint))
                    {
                        result.Add(interpolatedPoint);
                    }
                }
            }

            return result;
        }
    }
}