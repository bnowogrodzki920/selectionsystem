using System.Collections.Generic;
using UnityEngine;

namespace World.AStar
{
    public class AStarNode : MonoBehaviour
    {
        [SerializeField]
        private AStarController AStarController;
        
        [SerializeField]
        private List<AStarNode> _NeighbourNodes = new();
        public IReadOnlyList<AStarNode> NeighbourNodes => _NeighbourNodes;

        public Vector3 Position => transform.position;

        public float FScore => GScore + HScore;
        public float GScore { get; set; }
        public float HScore { get; set; }
        
        public AStarNode PreviousNode { get; set; }

        public bool IsWalkable;

        [SerializeField]
        private float _MaxDistanceToNeighbourNode = 3f;

        public void Initialize(AStarController aStarController, bool isWalkable)
        {
            AStarController = aStarController;
            IsWalkable = isWalkable;
            name = $"AStarNode {Position}";
        }
        
        public void AssignNeighbours()
        {
            foreach (var node in AStarController.Nodes)
            {
                if (node == this)
                {
                    continue;
                }
                
                // It could be more optimized with QuadTree
                if ((node.Position - Position).sqrMagnitude < _MaxDistanceToNeighbourNode * _MaxDistanceToNeighbourNode)
                {
                    _NeighbourNodes.Add(node);
                }
            }
        }

        #if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (UnityEditor.Selection.activeObject != gameObject)
            {
                return;
            }
            
            Gizmos.color = IsWalkable ? new Color(0, 0.75f, 0) : new Color(0.75f, 0, 0);
            Gizmos.DrawSphere(Position, 0.25f);

            foreach (var neighbourNode in _NeighbourNodes)
            {
                Gizmos.color = neighbourNode.IsWalkable ? new Color(0, 0.75f, 0.5f) : new Color(0.75f, 0, 0);
                Gizmos.DrawSphere(neighbourNode.Position, 0.2f);
            }
        }
        #endif
    }
}