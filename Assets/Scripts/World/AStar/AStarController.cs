using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Random = UnityEngine.Random;

namespace World.AStar
{
    public class AStarController : MonoBehaviour
    {
        [SerializeField]
        private List<AStarNode> _Nodes = new();
        public IReadOnlyList<AStarNode> Nodes => _Nodes;

        [SerializeField, Range(0, 1)]
        private float _NotWalkableNodeSpawnChance = 0.2f;

        [SerializeField]
        private Vector2Int _GridSize = new (30, 30);

        [SerializeField]
        private float _DistanceBetweenNodes = 1.5f;
        
        [SerializeField]
        private AssetReference _NodeAssetReference;

        public event Action OnNodesGenerated;
        
        private bool _NodesGenerated;
        
        public void Initialize()
        {
            GenerateNodes();
        }

        private async void GenerateNodes()
        {
            var loadAssetOperation = _NodeAssetReference.LoadAssetAsync<GameObject>();
            
            await loadAssetOperation.Task;

            var nodeAsset = loadAssetOperation.Result.GetComponent<AStarNode>();
            
            for (var x = 0; x < _GridSize.x; x++)
            {
                for (var y = 0; y < _GridSize.y; y++)
                {
                    var isWalkableNode = Random.value > _NotWalkableNodeSpawnChance;
                    var xPos = (x * _DistanceBetweenNodes) - (_GridSize.x * _DistanceBetweenNodes / 2f);
                    var zPos = (y * _DistanceBetweenNodes) - (_GridSize.y * _DistanceBetweenNodes / 2f);
                    var nodePos = new Vector3(xPos, 1, zPos);
                    
                    var node = Instantiate(nodeAsset, nodePos, Quaternion.identity, transform);
                    node.Initialize(this, isWalkableNode);
                    _Nodes.Add(node);
                }

                await Task.Yield();
            }

            foreach (var node in _Nodes)
            {
                node.AssignNeighbours();
            }
            
            OnNodesGenerated?.Invoke();
            _NodesGenerated = true;
        }

        public Path FindPath(Vector3 from, Vector3 to)
        {
            var startNode = FindClosestNode(from);
            var endNode = FindClosestNode(to);
            var closedSet = new HashSet<AStarNode>();
            var openSet = new List<AStarNode>() { startNode };

            if (!endNode.IsWalkable)
            {
                Debug.Log($"[{nameof(AStarController)}.{nameof(FindPath)}] Found End Node is not walkable! Trying to find path as close as possible to this node", endNode);
                endNode = endNode.NeighbourNodes.FirstOrDefault(node => node.IsWalkable);
                
                if (endNode == null)
                {
                    Debug.Log($"[{nameof(AStarController)}.{nameof(FindPath)}] Couldn't find close walkable node to {endNode}! Aborting...", endNode);
                    return null;
                }
            }
            
            while (openSet.Count > 0)
            {
                var bestF = 0;

                for (var i = 0; i < openSet.Count; i++)
                {
                    if (openSet[i].FScore < openSet[bestF].FScore)
                    {
                        bestF = i;
                    }
                }

                var currentNode = openSet[bestF];

                if (currentNode == endNode)
                {
                    Debug.Log($"[{nameof(AStarController)}.{nameof(FindPath)}] Found path from {from} to {to}!", endNode);
                    return RecreatePath(currentNode, to);
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                foreach (var neighbourNode in currentNode.NeighbourNodes)
                {
                    if (closedSet.Contains(neighbourNode) || !neighbourNode.IsWalkable)
                    {
                        continue;
                    }

                    var currentG = currentNode.GScore + GetHeuristicScore(currentNode, neighbourNode);
                    var foundNewPath = false;
                    
                    if (openSet.Contains(neighbourNode))
                    {
                        if (currentG < neighbourNode.GScore)
                        {
                            neighbourNode.GScore = currentG;
                            foundNewPath = true;
                        }
                    }
                    else
                    {
                        neighbourNode.GScore = currentG;
                        foundNewPath = true;
                        openSet.Add(neighbourNode);
                    }

                    if (!foundNewPath)
                    {
                        continue;
                    }
                    
                    neighbourNode.HScore = GetHeuristicScore(neighbourNode, endNode);
                    neighbourNode.PreviousNode = currentNode;
                }
            }
            
            Debug.LogError($"[{nameof(AStarController)}.{nameof(FindPath)}] Couldn't find path to {to}!", endNode);
            return null;
        }

        private Path RecreatePath(AStarNode currentNode, Vector3 endPoint)
        {
            var pathNodes = new List<AStarNode>();
            
            while (currentNode.PreviousNode != null)
            {
                if (pathNodes.Contains(currentNode.PreviousNode))
                {
                    break;
                }
                
                pathNodes.Add(currentNode.PreviousNode);
                currentNode = currentNode.PreviousNode;
            }
            
            pathNodes.Reverse();
            return new Path(pathNodes.Skip(1).ToList(), endPoint); // Can skip first node because it's almost equal to current starting position
        }

        private AStarNode FindClosestNode(Vector3 position)
        {
            var closestNode = _Nodes[0];
            var minSqrDist = float.MaxValue;
            
            foreach (var node in _Nodes)
            {
                var sqrDist = (node.Position - position).sqrMagnitude;
                
                if (sqrDist > 1 && sqrDist > minSqrDist)
                {
                    continue;
                }
                
                minSqrDist = sqrDist;
                closestNode = node;
            }

            return closestNode;
        }

        private float GetHeuristicScore(AStarNode aNode, AStarNode bNode)
        {
            return Vector3.Distance(aNode.Position, bNode.Position);
        }

        private void OnDrawGizmosSelected()
        {
            foreach (var node in _Nodes)
            {
                Gizmos.color = node.IsWalkable ? new Color(0, 0.75f, 0) : new Color(0.75f, 0, 0);
                Gizmos.DrawSphere(node.Position, 0.2f);
            }
        }

        public void Warp(Transform t)
        {
            var node = FindClosestNode(t.position);
            var warpedPosition = node.Position;
            t.position = warpedPosition;
        }

        public async Task Load((bool, Vector3)[] saveDataNodesData)
        {
            while (!_NodesGenerated)
            {
                await Task.Yield();
            }

            for (var i = 0; i < _Nodes.Count; i++)
            {
                _Nodes[i].Initialize(this, saveDataNodesData[i].Item1);
                _Nodes[i].transform.position = saveDataNodesData[i].Item2;
            }
        }
    }
}