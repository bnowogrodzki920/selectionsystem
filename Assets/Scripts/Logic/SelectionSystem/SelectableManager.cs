using UnityEngine;

namespace Logic.SelectionSystem
{
    [CreateAssetMenu(menuName = "SelectableManager", fileName = "SelectableManager")]
    public class SelectableManager : ScriptableObject
    {
        public ISelectable CurrentTarget { get; private set; }

        public void Select(ISelectable target)
        {
            if (ReferenceEquals(CurrentTarget, null))
            {
                Deselect();
            }
            
            if (ReferenceEquals(target, null))
            {
                return;
            }

            CurrentTarget = target;
            CurrentTarget.Select();
            
            Debug.Log($"[{nameof(SelectableManager)}.{nameof(Select)}] Selected {target.ID} as new target!", this);
        }

        public void Deselect()
        {
            CurrentTarget?.Deselect();
        }

        private void OnDisable()
        {
            CurrentTarget = null;
        }
    }
}