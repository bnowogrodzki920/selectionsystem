using System;

namespace Logic.SelectionSystem
{
    public interface ISelectable
    {
        string ID { get; }
        void Select();
        void Deselect();
        
        event Action OnSelected;
        event Action OnDeselected;
    }
}