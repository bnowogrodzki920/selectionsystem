using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Logic.Characters
{
    [CreateAssetMenu(fileName = "CharactersManager", menuName = "CharactersManager")]
    public class CharactersManager : ScriptableObject
    {
        [SerializeField]
        private CharactersManagerSettings Settings;

        [SerializeField]
        private List<CharacterModel> _AllCharacters = new();
        public IReadOnlyList<CharacterModel> AllCharacters => _AllCharacters;

        private List<CharacterModel> _SpawnedCharacters = new();
        public IReadOnlyList<CharacterModel> SpawnedCharacters => _SpawnedCharacters;

        public event Action OnCharactersSpawned;
        
        public void Initialize()
        {
            SpawnCharacters();
        }

        public void SpawnCharacters()
        {
            _SpawnedCharacters.Clear();

            if (AllCharacters.Count == 0)
            {
                Debug.LogError($"[{nameof(CharactersManager)}.{nameof(SpawnCharacters)}] There are no characters to spawn!");
            }
            
            if (AllCharacters.Count < Settings.MinimalCharacterCount)
            {
                Debug.LogError($"[{nameof(CharactersManager)}.{nameof(SpawnCharacters)}] There are too few characters to reach minimal character count: {Settings.MinimalCharacterCount}! There will be duplicates!");
            }
            
            var charactersCount = Random.Range(Settings.MinimalCharacterCount, AllCharacters.Count);

            for (var i = 0; i < charactersCount; i++)
            {
                var characterModel = _AllCharacters.GetRandomElement(character => !_SpawnedCharacters.Contains(character)) ?? AllCharacters.GetRandomElement();

                var movementSpeed = Random.Range(Settings.MovementSpeed.x, Settings.MovementSpeed.y);
                var maneuverability = Random.Range(Settings.Maneuverability.x, Settings.Maneuverability.y);
                var endurance = Random.Range(Settings.Endurance.x, Settings.Endurance.y);
                characterModel.Initialize(movementSpeed, maneuverability, endurance);
                
                _SpawnedCharacters.Add(characterModel);
            }
            
            OnCharactersSpawned?.Invoke();
            Debug.Log($"[{nameof(CharactersManager)}.{nameof(SpawnCharacters)}] Successfully spawned characters!", this);
        }

        public void AddCharacter(CharacterModel characterModel)
        {
            if (!_SpawnedCharacters.Contains(characterModel))
            {
                _SpawnedCharacters.Add(characterModel);
            }
        }

        public bool RemoveCharacter(CharacterModel characterModel)
        {
            return _SpawnedCharacters.Remove(characterModel);
        }
        
        public async Task Load((string, float, float, float)[] saveDataCharacterModelsData)
        {
            _SpawnedCharacters.Clear();

            foreach (var data in saveDataCharacterModelsData)
            {
                var characterModel = _AllCharacters.FirstOrDefault(model => model.ID == data.Item1);

                if (characterModel != null)
                {
                    characterModel.Initialize(data.Item2, data.Item3, data.Item4);
                }
                
                _SpawnedCharacters.Add(characterModel);
            }

            await Task.Yield();
            OnCharactersSpawned?.Invoke();
        }

        #if UNITY_EDITOR
        [ContextMenu("Refresh Assets")]
        public void RefreshAssets()
        {
            try
            {
                var assets = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(CharacterModel)}")
                    .Select(guid => UnityEditor.AssetDatabase.LoadAssetAtPath<CharacterModel>(UnityEditor.AssetDatabase.GUIDToAssetPath(guid)));
                _AllCharacters = assets.ToList();
                Debug.Log($"[{nameof(CharactersManager)}.{nameof(RefreshAssets)}] Successfully refreshed assets!", this);
            }
            catch (Exception e)
            {
                Debug.LogError($"[{nameof(CharactersManager)}.{nameof(RefreshAssets)}] Something went wrong during refreshing assets! Read the exception below: /n {e}", this);
            }
        }
        #endif
    }
}