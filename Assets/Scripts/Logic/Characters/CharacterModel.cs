using System;
using Logic.SelectionSystem;
using UnityEngine;

namespace Logic.Characters
{
    [CreateAssetMenu(fileName = "CharacterModel", menuName = "CharacterModel")]
    public class CharacterModel : ScriptableObject, ISelectable
    {
        [field: SerializeField]
        public Color Color { get; private set; }
        
        public float MovementSpeed { get; private set; }
        public float Maneuverability { get; private set; }
        public float Endurance { get; private set; }

        public string ID => name;
        
        public event Action OnSelected;
        public event Action OnDeselected;
        
        public void Initialize(float movementSpeed, float maneuverability, float endurance)
        {
            MovementSpeed = movementSpeed;
            Maneuverability = maneuverability;
            Endurance = endurance;
        }

        public void Select()
        {
            OnSelected?.Invoke();
        }

        public void Deselect()
        {
            OnDeselected?.Invoke();
        }
    }
}