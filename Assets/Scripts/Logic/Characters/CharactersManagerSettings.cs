using UnityEngine;

namespace Logic.Characters
{
    [CreateAssetMenu(fileName = "CharactersManagerSettings", menuName = "CharactersManagerSettings")]
    public class CharactersManagerSettings : ScriptableObject
    {
        [field: SerializeField]
        public int MinimalCharacterCount { get; private set; } = 3;
        
        [field: SerializeField]
        public Vector2 MovementSpeed { get; private set; }

        [field: SerializeField]
        public Vector2 Maneuverability { get; private set; }

        [field: SerializeField]
        public Vector2 Endurance { get; private set; }
    }
}
