using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Logic
{
    public static class Extensions
    {
        /// <summary>
        /// Returns random element from collection
        /// </summary>
        public static T GetRandomElement<T>(this IEnumerable<T> collection)
        {
            var enumerable = collection.ToList();
            return enumerable.ElementAt(Random.Range(0, enumerable.Count));
        }
        
        /// <summary>
        /// Returns random element from collection
        /// </summary>
        /// <returns>Returns random element from collection, if no element suits predicate - will return default value</returns>
        public static T GetRandomElement<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        {
            var enumerable = collection.Where(predicate).ToList();
            return enumerable.Count > 0 ? enumerable.ElementAt(Random.Range(0, enumerable.Count)) : default;
        }  
        
        public static float EaseInSine(float start, float end, float value)
        {
            end -= start;
            return -end * Mathf.Cos(value * (Mathf.PI * 0.5f)) + end + start;
        }

        public static float EaseOutSine(float start, float end, float value)
        {
            end -= start;
            return end * Mathf.Sin(value * (Mathf.PI * 0.5f)) + start;
        }
        
        public static float EaseInQuad(float start, float end, float value)
        {
            end -= start;
            return end * value * value + start;
        }

        public static float EaseOutQuad(float start, float end, float value)
        {
            end -= start;
            return -end * value * (value - 2) + start;
        }

        public static float EaseInOutQuad(float start, float end, float value)
        {
            value /= .5f;
            end -= start;
            if (value < 1) return end * 0.5f * value * value + start;
            value--;
            return -end * 0.5f * (value * (value - 2) - 1) + start;
        }
    }
}