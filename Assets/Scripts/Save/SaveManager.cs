using System;
using System.IO;
using System.Linq;
using Logic.Characters;
using Logic.SelectionSystem;
using Newtonsoft.Json;
using UnityEngine;
using World.AStar;
using World.CharacterPresenters;
using Path = System.IO.Path;

namespace Logic.Save
{
    public class SaveManager : MonoBehaviour
    {
        [SerializeField]
        private SelectableManager SelectableManager;
        
        [SerializeField]
        private CharactersManager CharactersManager;
        
        [SerializeField]
        private CharacterPresentersController CharacterPresentersController;
        
        [SerializeField]
        private AStarController AStarController;
        
        [SerializeField, Tooltip("It is related to Assets folder!")]
        private string _SavePath;
        
        [SerializeField]
        private string _SaveName = "SavedGame";

        [ContextMenu("Save")]
        public async void Save()
        {
            var saveData = new SaveData();

            if (SelectableManager != null)
            {
                saveData.GuideID = SelectableManager.CurrentTarget?.ID;
            }
            
            if (CharactersManager != null)
            {
                saveData.CharacterModelsData = CharactersManager.SpawnedCharacters.Select(model => (model.ID, model.MovementSpeed, model.Maneuverability, model.Endurance)).ToArray();
            }
            
            if (CharacterPresentersController != null)
            {
                saveData.CharacterPresentersTransformData = CharacterPresentersController.Presenters.Select(presenter =>
                {
                    var dummyVector = new DummyVector3()
                    {
                        x = presenter.transform.position.x,
                        y = presenter.transform.position.y,
                        z = presenter.transform.position.z,
                    };
                    var dummyQuaternion = new DummyQuaternion()
                    {
                        x = presenter.transform.rotation.x,
                        y = presenter.transform.rotation.y,
                        z = presenter.transform.rotation.z,
                        w = presenter.transform.rotation.w
                    };
                    return (presenter.Model.ID, dummyVector, dummyQuaternion);
                }).ToArray();
            }
            
            if (AStarController != null)
            {
                saveData.NodesData = AStarController.Nodes.Select(node =>
                {
                    var dummyVector = new DummyVector3()
                    {
                        x = node.Position.x,
                        y = node.Position.y,
                        z = node.Position.z,
                    };
                    return (node.IsWalkable, dummyVector);
                }).ToArray();
            }

            var saveString = JsonConvert.SerializeObject(saveData, Formatting.Indented);

            try
            {
                var path = Path.Combine(Application.dataPath, _SavePath);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                
                await using var writer = new StreamWriter(Path.Combine(path, $"{_SaveName}.txt"));
                await writer.WriteAsync(saveString);
                
                Debug.Log($"[{nameof(SaveManager)}.{nameof(Save)}] Successfully saved save file!", this);
            }
            catch (Exception e)
            {
                Debug.LogError($"[{nameof(SaveManager)}.{nameof(Save)}] Something went wrong during saving! Save file might be corrupted! Exception: \n{e}", this);
            }
        }

        [ContextMenu("Load")]
        public async void Load()
        {
            try
            {
                var path = Path.Combine(Application.dataPath, _SavePath, $"{_SaveName}.txt");

                if (!File.Exists(path))
                {
                    Debug.LogError($"[{nameof(SaveManager)}.{nameof(Save)}] There is no save file to load! Aborting...", this);
                    return;
                }
                
                using var reader = new StreamReader(path);
                var saveString = await reader.ReadToEndAsync();
                var saveData = JsonConvert.DeserializeObject<SaveData>(saveString);
                
                if (CharactersManager != null)
                {
                    await CharactersManager.Load(saveData.CharacterModelsData);
                }
                
                if (SelectableManager != null)
                {
                    var selectedCharacter = CharactersManager.SpawnedCharacters.FirstOrDefault(character => character.ID == saveData.GuideID);

                    if (selectedCharacter != null)
                    {
                        SelectableManager.Select(selectedCharacter);
                    }
                }

                if (CharacterPresentersController != null)
                {
                    var parsedSaveData = new (string, Vector3, Quaternion)[saveData.CharacterPresentersTransformData.Length];

                    for (var i = 0; i < saveData.CharacterPresentersTransformData.Length; i++)
                    {
                        var dummyVector = saveData.CharacterPresentersTransformData[i].Item2;
                        var vector = new Vector3(dummyVector.x, dummyVector.y, dummyVector.z);
                        
                        var dummyQuaternion = saveData.CharacterPresentersTransformData[i].Item3;
                        var quaternion = new Quaternion(dummyQuaternion.x, dummyQuaternion.y, dummyQuaternion.z, dummyQuaternion.w);

                        parsedSaveData[i] = (saveData.CharacterPresentersTransformData[i].Item1, vector, quaternion);
                    }
                    
                    await CharacterPresentersController.Load(parsedSaveData);
                }

                if (AStarController != null)
                {
                    var parsedSaveData = new (bool, Vector3)[saveData.NodesData.Length];

                    for (var i = 0; i < saveData.NodesData.Length; i++)
                    {
                        var dummyVector = saveData.NodesData[i].Item2;
                        var vector = new Vector3(dummyVector.x, dummyVector.y, dummyVector.z);

                        parsedSaveData[i] = (saveData.NodesData[i].Item1, vector);
                    }

                    await AStarController.Load(parsedSaveData);
                }
                
                Debug.Log($"[{nameof(SaveManager)}.{nameof(Load)}] Successfully loaded save file!", this);
            }
            catch (Exception e)
            {
                Debug.LogError($"[{nameof(SaveManager)}.{nameof(Load)}] Something went wrong during loading! Save file might be corrupted! Exception: \n{e}", this);
            }
        }

        private struct SaveData
        {
            public string GuideID;
            public (string, float, float, float)[] CharacterModelsData;
            public (string, DummyVector3, DummyQuaternion)[] CharacterPresentersTransformData;
            public (bool, DummyVector3)[] NodesData;
        }

        
        // These are here because JsonSerializer found self referencing loops
        private struct DummyVector3
        {
            public float x, y, z;
        }
        private struct DummyQuaternion
        {
            public float x, y, z, w;
        }
    }
}