using Logic.Characters;
using Logic.SelectionSystem;
using TMPro;
using UnityEngine;

namespace UI
{
    public class CharacterSelector : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _Text; 

        private SelectableManager SelectableManager;
        private CharacterModel _Model;

        public void Initialize(CharacterModel model, SelectableManager selectableManager)
        {
            SelectableManager = selectableManager;
            _Model = model;
            _Text.text = _Model ? _Model.name : "null";
        }
        
        public void Select()
        {
            if (_Model == null)
            {
                Debug.LogError($"[{nameof(CharacterSelector)}.{nameof(Select)}] Couldn't select model because it was null!", this);
                return;
            }
            
            SelectableManager.Select(_Model);
        }
    }
}