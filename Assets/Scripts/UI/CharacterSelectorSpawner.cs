using System.Collections.Generic;
using Logic.Characters;
using Logic.SelectionSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace UI
{
    public class CharacterSelectorSpawner : MonoBehaviour
    {
        [SerializeField]
        private CharactersManager CharactersManager;

        [SerializeField]
        private SelectableManager SelectableManager;

        [SerializeField]
        private AssetReference CharacterSelectorAssetReference;

        [SerializeField]
        private Transform CharacterSelectorsParent;
        
        private List<CharacterSelector> CharacterSelectors = new();
        
        private void Awake()
        {
            CharactersManager.OnCharactersSpawned += SpawnSelectors;
        }

        private async void SpawnSelectors()
        {
            // TODO add pooling
            if (CharacterSelectors.Count > 0)
            {
                for (var i = CharacterSelectors.Count - 1; i >= 0; i--)
                {
                    var selector = CharacterSelectors[i];
                    CharacterSelectors.Remove(selector);
                    Destroy(selector.gameObject);
                }
            }
            
            foreach (var characterModel in CharactersManager.SpawnedCharacters)
            {
                var instantiateOperation = CharacterSelectorAssetReference.InstantiateAsync(CharacterSelectorsParent);

                await instantiateOperation.Task;

                var selector = instantiateOperation.Result.GetComponent<CharacterSelector>();
                selector.Initialize(characterModel, SelectableManager);
                CharacterSelectors.Add(selector);
            }
        }

        private void OnDestroy()
        {
            CharactersManager.OnCharactersSpawned -= SpawnSelectors;
        }
    }
}